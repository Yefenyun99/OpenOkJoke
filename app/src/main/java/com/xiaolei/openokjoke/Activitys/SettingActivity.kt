package com.xiaolei.openokjoke.Activitys

import android.os.Bundle
import com.bumptech.glide.Glide
import com.xiaolei.easyfreamwork.common.listeners.Action
import com.xiaolei.openokjoke.Base.BaseActivity
import com.xiaolei.openokjoke.Exts.enqueue
import com.xiaolei.openokjoke.Net.APPNet
import com.xiaolei.openokjoke.Net.BaseRetrofit
import com.xiaolei.openokjoke.R
import com.xiaolei.openokjoke.Utils.VersionUtil
import kotlinx.android.synthetic.main.activity_setting.*
import java.io.File
import java.text.NumberFormat


/**
 * 设置界面
 * Created by xiaolei on 2018/3/15.
 */
class SettingActivity : BaseActivity()
{
    private val format = NumberFormat.getNumberInstance().apply {
        maximumFractionDigits = 2
    }
    private val mCacheDir by lazy {
        File(this.cacheDir, "image_manager_disk_cache")
    }

    private val appNet by lazy {
        BaseRetrofit.create(APPNet::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        setContentView(R.layout.activity_setting)
        super.onCreate(savedInstanceState)
    }

    override fun initObj()
    {

    }

    override fun initView()
    {
        Glide.with(this)
                .load(R.mipmap.ic_launcher)
                .into(show_app_icon)
    }

    override fun initData()
    {

    }

    override fun setListener()
    {
        app_title.setOnLeftImageClick { finish() }
        clear_cache_img.setOnClickListener {
            if (show_cache_memory.text != "0 M")
            {
                Alert("OK一笑 温馨提示", "图片缓存可以\n\n节省流量使用\n\n加快响应速度\n\n您是否确定清除缓存？"
                        , "否"
                        , null, "是"
                        , object : Action()
                {
                    override fun action()
                    {
                        deleteFolderFile(mCacheDir)
                        show_cache_memory.text = "${format.format(getFolderSize(mCacheDir) / 1024f / 1024f)} M"
                    }
                })
            }
        }

        about_me.setOnClickListener {
            val call = appNet.aboutme()
            call.enqueue(this, {
                if (it.isOk())
                {
                    Alert("OK一笑 关于我"
                            , it.content
                            , null
                            , null
                            , "确定"
                            , null)
                }
            })
        }
    }

    override fun loadData()
    {
        val versionCode = VersionUtil.getVersionCode(this)
        val versionName = VersionUtil.getVersionName(this)
        show_version.text = "V$versionCode\n代号:$versionName"

        if (mCacheDir.exists())
        {
            show_cache_memory.text = "${format.format(getFolderSize(mCacheDir) / 1024f / 1024f)} M"
        } else
        {
            show_cache_memory.text = "0 M"
        }
    }

    /**
     * 获取文件夹大小
     * @param file File实例
     * @return long
     */
    private fun getFolderSize(file: java.io.File): Long
    {
        var size: Long = 0
        val fileList = file.listFiles()
        for (i in fileList.indices)
        {
            size += if (fileList[i].isDirectory)
            {
                getFolderSize(fileList[i])
            } else
            {
                fileList[i].length()
            }
        }
        return size
    }


    /**
     * 删除指定目录下文件及目录
     */
    fun deleteFolderFile(dir: File)
    {
        if (dir.exists())
        {
            val files = dir.listFiles()
            files.forEach { file ->
                file.delete()
            }
        }
    }

}